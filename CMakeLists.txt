cmake_minimum_required(VERSION 3.25.0)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

project(gatsProject VERSION 0.1.0 LANGUAGES CXX)
add_executable(gatsProject main.cpp Game.cpp Hero.cpp)

find_package(SDL2 REQUIRED)
find_package(SDL2_IMAGE REQUIRED)
include_directories( "/opt/homebrew/include")
target_link_libraries( gatsProject ${SDL2_LIBRARIES} SDL2_image::SDL2_image)
file(COPY resources DESTINATION ${CMAKE_BINARY_DIR})
 