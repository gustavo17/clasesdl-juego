#pragma once

#include "SDL2/SDL_render.h"
#include <SDL2/SDL.h>
#include <vector>
#include "GameObject.h"

class Game {
    public:
        Game();                 // constructor
        virtual ~Game();        // destructor;

        //precaucion de que no se copie el objeto
        //asigna dinamicamente memoria 
        //por el objeto window y el objeto renderer
        Game(const Game & src)=delete; //explicitamente eliminando constructor de copia
        Game(Game &&src)=delete; //inhabilita el constructor de move
        Game & operator=(const Game &rhs)=delete; //inhabilita la copia por asignacion
        Game & operator=(Game && rhs)=delete; //inhabilita la copia por asignacion de movimiento

        void init();            // inicializacion de juego
        void eventsHandler();   // eventos que informan que el usuario hace algo
        void update(double elapsedTime); // actualizacion de la logica del juego
        void render() const;
        void release();     // liberacion de recursos
        bool isRunning() const; // consulta si el juego esta corriendo

        //agrega un GameObject al vector listaDeObjectos
        //el vector es privado, debe disponibilzarse a traves de un metodo public.
        //@param ptr, es una referecia a un GameObject
        //la responsabilidad de gestion de memoria reside en el que invoca a la funcion
        void addGameObject(GameObject& go);

      private:
        bool m_running;
        SDL_Window* m_window=nullptr;
        SDL_Renderer* m_renderer=nullptr;
        
        //no puede almacenar objectos de tipo GameObject porque es clase abstracta
        //no se pueden construir objectos GameObject solo sirven los punteros 
        //pero si puede almacenar punteros a GameObject (clase padre Generica de todos los Objetos del Juego)
        // Hero es un GameObject (se puede crear un Hero y usarlo como puntero a Game Object)
        //ver main antes del init
        std::vector< GameObject * > listaDeObjetos; 
};