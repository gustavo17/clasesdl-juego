#include <chrono>
#include <thread>
#include "Game.h"
#include "Hero.h"

int main(){

    //GAME LOOP
    const double TIEMPO_ESPERADO=0.01;
    Game g; //constructor normal

    //implementa un Hero en el stack
    //la ventaja es que al terminar main se libera automaticamente
    //(alternativa a smartPointer es viable pero crearia Hero en heap en vez de stack)
    //por simplicidad , mejor del stack (por ahora)
    Hero h{0,100};
    Hero h2{500,150};
    Hero h3{900,200};


    // transfiere la direccion de Hero para su uso al interior del objecto Game
    //ver Game.h (declaracion)
    g.addGameObject(h); 
    g.addGameObject(h2); 
    g.addGameObject(h3); 


    //inicializar todo
    g.init(); //inicializacion de la libreria grafica
    auto previous=std::chrono::steady_clock::now();
    while (g.isRunning()){
        g.eventsHandler();
        auto currentTime=std::chrono::steady_clock::now();
        double diferencia=std::chrono::duration_cast<std::chrono::duration<double>>(currentTime-previous).count();
        //si el ciclo demora mas , entonces dejo pasar sin esperar
        if (diferencia<TIEMPO_ESPERADO){
            int esperar=static_cast<int>((TIEMPO_ESPERADO-diferencia)*1000000000); //conversion entera a millisegundos
            std::this_thread::sleep_for(std::chrono::nanoseconds(esperar));
            currentTime=std::chrono::steady_clock::now();
            diferencia=std::chrono::duration_cast<std::chrono::duration<double>>(currentTime-previous).count();
        }
        previous=currentTime;
        g.update(diferencia); //update se ejecute a intervalos regulares
        g.render();
    }
    g.release();

}