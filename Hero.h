#pragma once
#include <string>
#include "GameObject.h"
#include "SDL2/SDL_render.h"


class Hero : public GameObject{ //Herencia
    public:
      Hero(int x, int y);
      void init(SDL_Renderer * renderer) override;
      void update(double elapsedTime) override;
      void render() const override;
      void release() override;

    private:
        int m_posyInicial;
        int m_posXInicial;
        SDL_Texture *m_texture;
        SDL_Renderer *m_renderer;
        SDL_Rect m_sourceRect;
        SDL_Rect m_destRect;
        const static inline std::string m_path {"resources/hero.png"};
        double tiempo=0;
        int frameAmostrar=0;

};