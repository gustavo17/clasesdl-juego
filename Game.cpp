#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "Game.h"
#include "Parametros.h"

Game::Game():m_running{true}{};

Game::~Game(){

};

void Game::init(){
    std::cout << "inicializando SDL ..." << std::endl;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        throw std::runtime_error (SDL_GetError());
    }    

    m_window = SDL_CreateWindow("Juego", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                          Parametros::SCREEN_WIDTH, Parametros::SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (m_window == nullptr) {
        throw std::runtime_error (SDL_GetError());
    }

    m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
    if (m_renderer == nullptr) {
        throw std::runtime_error (SDL_GetError());
    }


    //libreria para decodificacion de distintos tipos de imagenes
    int imgFlags = IMG_INIT_PNG; // Puedes especificar otros formatos aquí
    if (!(IMG_Init(imgFlags) & imgFlags)) {
        throw std::runtime_error (SDL_GetError());
    }    

    //setea color de background // R G B ALPHA
     SDL_SetRenderDrawColor(m_renderer, 0xFF, 0x00, 0x00, 0xFF);

   
    //inicializo cada objeto
     for (auto & go : listaDeObjetos){
        go->init(m_renderer);
     }

};

void Game::eventsHandler(){
    //evento de salida de la ventana
    SDL_Event e;
    if  (SDL_PollEvent(&e) != 0) {
        if (e.type == SDL_QUIT) {
            m_running=false;
        }
    }
};

void Game::update(double elapsedTime){
    //std::cout<<"time:"<<elapsedTime<<std::endl;

    for (auto & go:listaDeObjetos){
        go->update(elapsedTime);
    }
};

void Game::render() const{
    //borra la pantalla (a rojo)
    SDL_RenderClear(m_renderer);

    //dibujo los game objects
    //
    for (auto & go :listaDeObjetos){
        go->render();
    }

    //presentar el renderer en pantalla
    SDL_RenderPresent(m_renderer);
};

void Game::release(){
    for (auto & go: listaDeObjetos){
        go->release();
    }

    //borrar el renderer
    std::cout << "liberando recursos SDL ..." << std::endl;
    SDL_DestroyRenderer(m_renderer);
    m_renderer=nullptr;

    //borrar el window
    SDL_DestroyWindow(m_window);
    m_window=nullptr;

    //baja la libreria SDL
    IMG_Quit();
    SDL_Quit(); //bajo una referencia
};

bool Game::isRunning() const { return m_running; };

void Game::addGameObject(GameObject& go) {
    //se agrega la referecia de go (la lista almacena punteros)
    listaDeObjetos.push_back(&go);
}
