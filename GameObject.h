#pragma once

//clase Abstracta
#include "SDL2/SDL_render.h"
class GameObject {
    public:
        GameObject()=default;
        virtual ~GameObject()=default;
        virtual void init(SDL_Renderer * renderer)=0; //definicion de un metodo abstracto (SIN IMPLEMENTACION)
        virtual void update(double elapsedTime)=0;
        virtual void render() const=0;
        virtual void release()=0;
};