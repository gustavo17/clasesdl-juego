#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "Hero.h"
#include "Parametros.h"
#include "SDL2/SDL_pixels.h"
#include "SDL2/SDL_render.h"

void Hero::init(SDL_Renderer * renderer){
    std::cout<<"Iniciando Hero"<<std::endl;
    m_renderer=renderer;
    
    SDL_Surface* loadedSurface = IMG_Load(m_path.c_str());
    if (loadedSurface == nullptr) {
        throw std::runtime_error("no se pudo cargar imagen");
    }
    
    m_texture = SDL_CreateTextureFromSurface(m_renderer, loadedSurface);
    if (m_texture == nullptr) {
        throw std::runtime_error( SDL_GetError());
    }
    SDL_FreeSurface(loadedSurface); //libero la surface

    int textureWidth = 0;
    int textureHeight = 0;
    SDL_QueryTexture(m_texture, NULL, NULL, &textureWidth, &textureHeight);
    textureWidth=textureWidth/10; //10 frames

    m_sourceRect = SDL_Rect {0, 0, textureWidth, textureHeight};
    m_destRect = SDL_Rect {m_posXInicial,m_posyInicial,textureWidth,textureHeight};

};

//cerebro
void Hero::update(double elapsedTime){
    tiempo+=elapsedTime;
    m_destRect.x+=elapsedTime*300;
    if (m_destRect.x>Parametros::SCREEN_WIDTH)m_destRect.x=-m_destRect.w;
    //std::cout<<"AQUI>>>> "<<elapsedTime<<", "<<tiempo<<std::endl;
    if (tiempo>0.1){
        tiempo-=0.1; //resta 0.1
        frameAmostrar++;
        if (frameAmostrar==10)frameAmostrar=0;
        m_sourceRect.x=m_sourceRect.w*frameAmostrar;
    }
};

//dibujo
void Hero::render() const {
    SDL_RenderCopyEx(m_renderer,m_texture, &m_sourceRect, &m_destRect,0,nullptr,SDL_FLIP_NONE);
};
void Hero::release(){
    std::cout<<"liberando memoria de Hero"<<std::endl;
   SDL_DestroyTexture(m_texture);
    m_texture=nullptr;
    m_renderer=nullptr;    
};

Hero::Hero(int x, int y):m_posXInicial{x},m_posyInicial{y} {

}
